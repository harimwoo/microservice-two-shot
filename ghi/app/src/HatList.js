import React, { useState, useEffect } from 'react'

function HatColumn(props){
    const handleDelete = async (hat) => {
        const url = `http://localhost:8090/${hat}`
        await fetch(url, {method:'delete'})
        props.onHatDeleted(hat)
    }
    return (
        <div className='col'>
            {props.list.map(data => {
                const hat = data.hat
                return (
                    <div key={hat.href} className="card mb-3 shadow">
                        <img src={hat.picture_URL} className="card-img-top" />
                        <div className='card-body'>
                            <h5 className="card-title">{hat.style_name}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">
                                color: {hat.color}
                                fabric: {hat.fabric}
                            </h6>
                            <p className="card-text">
                                Closet: {hat.location.closet_name}
                                Section: {hat.location.section_number}
                                Shelf: {hat.location.shelf_number}
                            </p>
                            <button
                                id={hat.href}
                                className="btn btn-danger"
                                onClick={() => handleDelete(hat.href)}
                            >
                                delete
                            </button>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}



function HatList(props){

    const [hatColumns, setHatColumns] = useState([[],[],[]])

    const fetchData = async () => {
        const url = "http://localhost:8090/api/hats"

        try {
            const response = await fetch(url)
            if (response.ok) {
                const data = await response.json()

                const requests = []
                for(let hat of data.hats){
                    const detailUrl = `http://localhost:8090${hat.href}`
                    requests.push(fetch(detailUrl))
                }

                const responses = await Promise.all(requests)

                const columns = [[],[],[]]

                let i = 0
                for (const hatResponse of responses){
                    if(hatResponse.ok){
                        const details = await hatResponse.json()
                        columns[i].push(details)
                        i++
                        if(i > 2){
                            i = 0
                        }
                    } else {
                        console.error(hatResponse)
                    }
                }
                setHatColumns(columns)
            }
        } catch (e) {
            console.error(e)
        }
    }

    useEffect(()=>{
        fetchData()
    },[])

    const handleHatDeleted = (deletedHat) => {
        const newHatColumns = hatColumns.map((hatList) => {
            return hatList.filter((hat) => hat.hat.href !== deletedHat)
        })
        setHatColumns(newHatColumns)
    }

    return(
        <>
        <div className="container">
            <h2>Hats</h2>
            <div className="row">
                {hatColumns.map((hatList, index) => {
                    return(
                        <HatColumn key={index} list={hatList} onHatDeleted={handleHatDeleted} />
                    )
                })}
            </div>

        </div>
        </>

    )
}

export default HatList
