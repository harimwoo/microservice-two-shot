import React, { useEffect, useState } from "react"

function ShoeForm(props){
    const [closetNames, setClosetNames] = useState([])
    const [model, setModel] = useState('')
    const [manufacturer, setManufacturer] = useState('')
    const [color, setColor] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [bin, setBin] = useState('')

    const handleBinChange = (event) => {
        const value = event.target.value
        setBin(value)
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }
    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }
    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value)
    }

    const handleSubmit = async(event) => {
        event.preventDefault()

        const data = {}
        data.bin = bin
        data.model_name = model
        data.color = color
        data.manufacturer = manufacturer
        data.picture_url = pictureUrl
        console.log(data)
        const shoeUrl = 'http://localhost:8080/api/shoes/'
        const config = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(shoeUrl, config)
        if(response.ok){
            const newShoe = await response.json()
            console.log(newShoe)
            setManufacturer('')
            setModel('')
            setColor('')
            setPictureUrl('')
            setBin('')
        }
    }

const fetchData = async () => {
    const url = "http://localhost:8100/api/bins"
    const response = await fetch(url)
    if(response.ok){
        const closetNames = []
        const data = await response.json()
        setClosetNames(data["bins"])
    }
}
useEffect(()=>{
    fetchData();
}, [])
    return(
        <div className="row">
            <div className="offset-3 col-6">
                <h1>Create a new Shoe</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" value={manufacturer}/>
                        <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleModelChange} placeholder="Model" required type="text" name="model" id="model" className="form-control" value={model}/>
                        <label htmlFor="model">Model</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color}/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePictureUrlChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" value={pictureUrl}/>
                        <label htmlFor="picture_url">Picture URL</label>
                    </div>
                    <div className="mb-3">
                        <select value={bin} onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                            <option value="">Choose a Bin</option>
                            {closetNames.map(closetName => {
                                return (
                                    <option value={closetName.href} key={closetName.id}>{closetName.closet_name} Bin #: {closetName.bin_number}
                                     Size: {closetName.bin_size}</option>
                                )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    )
}

export default ShoeForm





