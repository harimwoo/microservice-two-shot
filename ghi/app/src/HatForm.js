import React, { useEffect, useState } from 'react'

function HatForm(props){
    const [closetNames, setClosetNames] = useState([])
    const [fabric, setFabric] = useState('')
    const [style, setStyle] = useState('')
    const [color, setColor] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [location, setLocation] = useState('')

    const handleLocationChange = (event) => {
        const value = event.target.value
        setLocation(value)
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }
    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }
    const handleStyleChange = (event) => {
        const value = event.target.value
        setStyle(value)
    }
    const handleFabricChange = (event) => {
        const value = event.target.value
        setFabric(value)
    }

    const handleSubmit = async(event) => {
        event.preventDefault()

        const data = {}
        data.location = location
        data.style_name = style
        data.color = color
        data.fabric = fabric
        data.picture_URL = pictureUrl

        const hatUrl = 'http://localhost:8090/api/hats/'
        const config = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(hatUrl, config)
        if(response.ok){
            const newHat = await response.json()
            setFabric('')
            setStyle('')
            setColor('')
            setPictureUrl('')
            setLocation('')
        }
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/"
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            setClosetNames(data["locations"])
        }
    }

    useEffect(()=>{
        fetchData();
    }, [])
    return(
        <div className='row'>
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={fabric}/>
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStyleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" value={style}/>
                            <label htmlFor="style">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color} />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} placeholder="Picture Url" required type="text" name="picture_URL" id="picture_URL" className="form-control" value={pictureUrl} />
                            <label htmlFor="picture_URL">Picture Url</label>
                        </div>
                        <div className="mb-3">
                            <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {closetNames.map(closetName => {
                                    return (
                                        <option value={closetName.href} key={closetName.id}>{closetName.closet_name} shelf: {closetName.shelf_number} section: {closetName.section_number}</option>
                                    )
                                })}
                            </select>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HatForm
