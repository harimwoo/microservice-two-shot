import React, { useState, useEffect } from "react"

function ShoeColumn(props){
    const handleDelete = async (shoe) => {
        const url = `http://localhost:8080/${shoe}`
        await fetch(url, {method:'delete'})
        props.onShoeDeleted(shoe)
    }
    return (
        <div className='col'>
            {props.list.map(data => {
                const shoe = data.shoe
                return (
                    <div key={shoe.href} className="card mb-3 shadow">
                        <img src={shoe.picture_url} className="card-img-top" />
                        <div className='card-body'>
                            <h5 className="card-title">{shoe.model_name}</h5>
                            <h6 className="card-text">
                                color:{shoe.color}
                                manufacturer:{shoe.manufacturer}
                            </h6>
                            <p className="card-text">
                                Closet: {shoe.bin.closet_name}
                                Bin Number: {shoe.bin.bin_number}
                                Bin Size: {shoe.bin.bin_size}
                            </p>
                            <button
                                id={shoe.href}
                                className="btn btn-danger"
                                onClick={() => handleDelete(shoe.href)}
                            >
                                delete
                            </button>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

function ShoeList(props) {
    
    const [shoeColumns, setShoeColumns] = useState([[],[],[]])

    const fetchData = async () => {
    
        const url = "http://localhost:8080/api/shoes";
        try {
            const response = await fetch(url);
            if (response.ok){
                const data = await response.json()

                const requests = []

                for(let shoe of data.shoes){
                    const detailUrl = `http://localhost:8080${shoe.href}`
                    requests.push(fetch(detailUrl))
                }

                const responses = await Promise.all(requests)

                const columns = [[],[],[]]

                let i = 0
                for (const shoeResponse of responses){
                    if(shoeResponse.ok) {
                        const details = await shoeResponse.json()
                        columns[i].push(details)
                        i++
                        if(i > 2){
                            i = 0
                        }
                    } else {
                        console.error(shoeResponse)
                    }
                }
                setShoeColumns(columns)
            }
            } catch (e) {
                console.error(e)
            }
    }
    useEffect(()=>{
        fetchData()
    },[])
    const handleShoeDeleted = (deletedShoe) => {
        const newShoeColumns = shoeColumns.map((shoeList) => {
            return shoeList.filter((shoe) => shoe.shoe.href !== deletedShoe)
        })
        setShoeColumns(newShoeColumns)
    }
    

    return(
        <>
        <div className="container">
            <h2>Shoes</h2>
            <div className="row">
                {shoeColumns.map((shoeList, index) => {
                    return(
                        <ShoeColumn key={index} list={shoeList} onShoeDeleted={handleShoeDeleted}/>
                    )
                })}
            </div>

        </div>
        </>

    )
}


export default ShoeList;
