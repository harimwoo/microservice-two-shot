# Wardrobify

Team:

* Joseph Keene  - Shoes Microservice
* Harim Woo - Hat Microservice

## Design

## Shoes microservice
Shoe model: BinVO, Manufacturer, Model Name, Color, Picture URL, Bin in the Wardrobe

Shoe Microservice will have a BinVO that will be polling from Wardrobe/Bin
## Hats microservice

Hat model: LocationVO, Fabric, Style Name, Color, Picture URL, Location in the Wardrobe

Hat Microservice will have a LocationVO that will be polling from Wardrobe/Location
