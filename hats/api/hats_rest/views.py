from django.shortcuts import render
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number"
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "fabric",
        "color",
        "location",
        "picture_URL"
    ]
    encoders = {
        "location": LocationVODetailEncoder()
    }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "fabric",
        "color",
        "location",
        "picture_URL"
    ]
    encoders = {
        "location": LocationVODetailEncoder()
    }



@require_http_methods(["GET", "POST"])
def list_hats(request, location_vo_id=None):
    """
    GET or POST hats
    """
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats}, encoder=HatListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "INVALID LOCATION ID"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse({"hat": hat}, encoder=HatDetailEncoder)


@require_http_methods(["GET", "DELETE"])
def show_hat(request, id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse({"hat": hat}, HatDetailEncoder)
        except Hat.DoesNotExist:
            return JsonResponse({"message": "INVALID HAT ID"})
    else:
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
