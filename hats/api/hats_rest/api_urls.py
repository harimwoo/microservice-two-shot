from django.urls import path

from .views import list_hats, show_hat

urlpatterns = [
    path("hats/", list_hats, name="create_hat"),
    path("hats/<int:id>/", show_hat, name="show_hat")
]
