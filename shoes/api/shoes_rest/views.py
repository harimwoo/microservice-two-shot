from django.shortcuts import render
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json
# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "bin",
        "picture_url",   
    ]
    encoders = {
        "bin":BinVODetailEncoder()
    }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        'bin': BinVODetailEncoder()
        }


@require_http_methods(["GET", "POST"])
def list_shoes(request, bin_vo_id=None):
    """
    GET and POST shoes
    """
    if request.method == "GET":
        if bin_vo_id is not None:
            shoe = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes}, encoder=ShoeListEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "NOT VALID"}
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse({"shoe": shoe}, encoder=ShoeDetailEncoder)
        
        
    
@require_http_methods(["GET", "DELETE"])
def show_shoe(request, id):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=id)
            return JsonResponse({"shoe": shoe}, ShoeDetailEncoder)
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "INVALID"})
    else:
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})